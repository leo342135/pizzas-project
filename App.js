/**
 * Pizzas Test Project React Native App.
 * @author lgutierrez
 */

import React, {Component} from 'react';
import RootStack from './src/config/routingConfig';
import {Provider} from 'react-redux';
import configureStore from './src/configureStore'

let store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootStack />
      </Provider>
    );
  }
}