# Pizzas Project

Test Project for SupperClub

# Generals
This project consists of a React Native Mobile App which complies with the "Pizza delivery app" specifications.

It was tested in an **iPhone X** emulator and in an **Android 5.1 720 x 1280** emulator provided by Android Studio.

# Hardware setup
* Macbook Air 13"
* MacOS High Sierra 10.13.6
* 1.8 GHz Intel Core i5 processor
* 8GB RAM

# Software setup
* **Xcode** v9.2
* **Android Studio** v3.2.1, JDK 1.8.0_152
* **Gradle** v4.10.2
* **React** v16.8.3
* **React Native** v0.59.8

# Application design
The general design visualized for this app is basically a React Native app aimed for iOS and Android.
It is using [Redux](https://github.com/reduxjs/redux) for state management and [Redux Thunk](https://github.com/reduxjs/redux-thunk) for async operations such as API calls.
[React Navigation](https://reactnavigation.org/) was also imported for screen navigation.

The archtype used was to place all of the React and React Native code within the *root directory* **src**. Only the main entry point of the app in App.js was placed out of this directory.

Inside that folder we will find the store configuration and more directories based on separation of concerns such as:
*  configurations (like the routing of the screens)
*  components (screens and display components)
*  actions (for linking components and redux reducers)
*  reducers (for state data management)
*  services (for API calls)
*  constants (for centralizing actions and reducers functions)

There is two main screens, reducers and actions dispatchers used:
1.  Menu. In here, the menu list is retrieved from the API endpoint and displayed.
2.  Order. In here, we will find an object for the current selected order in the main screen and the confirmed order once the user taps the "Finish Order" button.

The order confirmation reducer is an API mock call, considering that the scope was only the React Native app and not a Back-End.