/**
 * Actions middleware file
 * @author lgutierrez
 */
import { bindActionCreators } from 'redux'
import * as MenuActions from './menu.actions';
import * as OrderActions from './order.actions';

export const ActionCreators = Object.assign({},
    MenuActions,
    OrderActions,
)

export const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(ActionCreators, dispatch);
}