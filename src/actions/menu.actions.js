/**
 * Menu actions
 * @author lgutierrez
 */
import { GET_MENU, GET_MENU_SUCCESS, GET_MENU_FAILURE } from '../constants';
import { fetchMenuFromApi } from './../services/menu';

export const getMenu = () => {
    return {type: GET_MENU}
}

export const getMenuSuccess = (payload) => {
    return {type: GET_MENU_SUCCESS, payload}
}

export const getMenuFailure = () => {
    return {type: GET_MENU_FAILURE}
}

export const fetchMenu = () => {
    return (dispatch) => {
        
        dispatch(getMenu());

        fetchMenuFromApi()
        .then(([response, json]) => {
            dispatch(getMenuSuccess(json));
        })
        .catch((error) => {
            dispatch(getMenuFailure);
            console.log(error);
        })
    }
}