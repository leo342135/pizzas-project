/**
 * Order actions
 * @author lgutierrez
 */
import {
    SELECT_ORDER,
    SELECT_ORDER_SUCCESS,
    SELECT_ORDER_FAILURE,
    CONFIRM_ORDER,
    CONFIRM_ORDER_SUCCESS,
    CONFIRM_ORDER_FAILURE,
    WILL_ADD_EXTRA_INGREDIENT,
    ADD_EXTRA_INGREDIENT,
} from './../constants';

export const selectOrder = (payload) => {
    return {type: SELECT_ORDER, payload}
}

export const selectOrderSuccess = () => {
    return {type: SELECT_ORDER_SUCCESS}
}

export const selectOrderFailure = () => {
    return {type: SELECT_ORDER_FAILURE}
}

export const confirmOrder = (payload) => {
    return {type: CONFIRM_ORDER, payload}
}

export const confirmOrderSuccess = () => {
    return {type: CONFIRM_ORDER_SUCCESS}
}

export const confirmOrderFailure = () => {
    return {type: CONFIRM_ORDER_FAILURE}
}

export const willAddExtraIngredient = () => {
    return {type: WILL_ADD_EXTRA_INGREDIENT}
}

export const addExtraIngredient = (payload) => {
    return {type: ADD_EXTRA_INGREDIENT, payload}
}

export const selectOrderToApi = (payload) => {
    return (dispatch) => {
        
        dispatch(selectOrder(payload));

        /** Select the order in memory for demo purposes,
         * but this should be an API call for temporary storage (shopping cart) */
        new Promise((resolve)=>{
            resolve();
        }).then(()=>{
            dispatch(selectOrderSuccess());
        });
    }
}

export const confirmOrderToApi = (payload) => {
    return (dispatch) => {
        
        dispatch(confirmOrder(payload));

        /** Confirm the order in memory for demo purposes,
         * but this should be an API call to save the order */
        new Promise((resolve)=>{
            resolve();
        }).then(()=>{
            dispatch(confirmOrderSuccess());
        });
    }
}