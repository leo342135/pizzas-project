/**
 * Main screen component of the app
 * @author lgutierrez
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { mapDispatchToProps } from './../../actions';

class MenuScreen extends Component {

    constructor(props) {
      super(props);
      this.selectIngredient = this.selectIngredient.bind(this);
      this.confirmOrder = this.confirmOrder.bind(this);
      this.addIngredient = this.addIngredient.bind(this);
    }

    componentDidMount(){
      this.props.fetchMenu();
    }

    renderMenuList(){      
      const menuList = this.props.menu.list;
      if (menuList.length > 0) {
        return menuList
          .map((menuItem, index) => {
              return (                        
                <View key={index} style={styles.menuList}>
                  <Text>
                    {menuItem.name}
                  </Text>
                  <Text>
                    {menuItem.price}
                  </Text>
                  <Button
                    onPress={()=> this.selectIngredient(menuItem)}
                    title="Select"
                    color="#841584"
                  />
                </View>
              );
          });
      }
    }

    renderSelectedOrder(){
      return (
        <View>
          <Text>Selected pizza: { this.props.order.currentOrder.name }</Text>
          <Text>Price: { ' $ ' + this.props.order.currentOrder.price }</Text>
        </View>
      )
    }

    selectIngredient(ingredientType){
      if(this.props.order.willAddExtraIngredient){
        if(ingredientType.name === this.props.order.currentOrder.name){
          alert('Please select a different ingredient than the one already selected.')
        }else{
          this.props.addExtraIngredient(ingredientType);
        }
      }else{
        this.props.selectOrderToApi(ingredientType);
      }
    }

    addIngredient(){
      this.props.willAddExtraIngredient();
      alert('Please select the extra ingredient from the list.');
    }

    confirmOrder(){
      this.props.confirmOrderToApi(this.props.order.currentOrder);
      this.props.navigation.navigate('OrderConfirmationScreen')
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Hello Pizza customer</Text>
                {
                  this.props.menu.isFetching &&
                  <Text>Loading</Text>
                }
                {
                  this.props.menu.error &&
                  <Text>Error getting the menu list, please try again later.</Text>
                }
                <ScrollView contentContainerStyle={styles.menuWrapper}>
                {
                  this.props.menu.list.length > 0 &&
                  this.renderMenuList()
                }
                </ScrollView>
                {
                  this.props.order.currentOrder.name &&
                  this.renderSelectedOrder()
                }
                {
                  this.props.order.error &&
                  <Text>Error selecting order, please try again later.</Text>
                }
                <Button 
                  onPress={this.addIngredient}
                  title="Add extra ingredient"
                  color="#841584"
                  disabled={!this.props.order.currentOrder.name}
                />
                <Button 
                  onPress={this.confirmOrder}
                  title="Finish Order"
                  color="#841584"
                  disabled={!this.props.order.currentOrder.name}
                />
            </View>
        );
    }
}

const mapStateToProps = state => {
  return {
    menu: state.menu,
    order: state.order
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen);

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    title: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    menuList:{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
    },
    menuWrapper:{
      flex: 1,
      flexGrow: 1,
    }
  });