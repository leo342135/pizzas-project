/**
 * Order confirmation after menu and flavor completion
 * @author lgutierrez
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { connect } from 'react-redux';
import { mapDispatchToProps } from './../../actions';

class OrderConfirmationScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Order successfully sent!</Text>
                {
                  this.props.order.confirmedOrder && 
                  <View style={styles.menuList}>
                    <Text>{this.props.order.confirmedOrder.name}</Text>
                    <Text>{' $ ' + this.props.order.confirmedOrder.price}</Text>
                  </View>
                }
            </View>
        );
    }
}

const mapStateToProps = state => {
  return {
    order: state.order
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderConfirmationScreen);

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    title: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    menuList:{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'stretch',
    },
  });