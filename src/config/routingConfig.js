/**
 * Routing configuration of the screens
 * @author lgutierrez
 */

import { createStackNavigator, createAppContainer } from 'react-navigation';
import MenuScreen from './../components/menu/MenuScreen';
import OrderConfirmationScreen from './../components/order/OrderConfirmationScreen';

const AppNavigator = createStackNavigator({
    MenuScreen,
    OrderConfirmationScreen
});

const RootStack = createAppContainer(AppNavigator);
  
export default RootStack;