/**
 * Redux store configuration, including the reducers' index and thunk for async processing.
 * @author lgutierrez
 */

import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';

export default configureStore = () => {
    let store = createStore(reducers, applyMiddleware(thunk));
    return store;
}