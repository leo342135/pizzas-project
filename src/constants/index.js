/**
 * Constant values for use in redux reducers and actions
 * @author lgutierrez
 */

/**
 * Menu and flavor constants
 */
export const GET_MENU = 'GET_MENU';
export const GET_MENU_SUCCESS = 'GET_MENU_SUCCESS';
export const GET_MENU_FAILURE = 'GET_MENU_FAILURE';

/**
 * Order constants
 */
export const SELECT_ORDER = 'SELECT_ORDER';
export const SELECT_ORDER_SUCCESS = 'SELECT_ORDER_SUCCESS';
export const SELECT_ORDER_FAILURE = 'SELECT_ORDER_FAILURE';
export const CONFIRM_ORDER = 'CONFIRM_ORDER';
export const CONFIRM_ORDER_SUCCESS = 'CONFIRM_ORDER_SUCCESS';
export const CONFIRM_ORDER_FAILURE = 'CONFIRM_ORDER_FAILURE';
export const WILL_ADD_EXTRA_INGREDIENT = 'WILL_ADD_EXTRA_INGREDIENT';
export const ADD_EXTRA_INGREDIENT = 'ADD_EXTRA_INGREDIENT';