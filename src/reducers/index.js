/**
 * Combines all the reducers into one, so that only one import is needed in the components.
 * @author lgutierrez
 */

import {combineReducers} from 'redux';
import menuReducer from './menu.reducer';
import orderReducer from './order.reducer';

export default combineReducers({
    menu: menuReducer,
    order: orderReducer,
});