/**
 * Menu data reducer
 * @author lgutierrez
 */

import { GET_MENU, GET_MENU_SUCCESS, GET_MENU_FAILURE } from './../constants';

const initialState =  {
    list: [],
    isFetching: false,
    error: false,
}

export default menuReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_MENU:
            return {
                ...state,
                list: [],
                isFetching: true,
            };
        case GET_MENU_SUCCESS:
            return {
                ...state,
                list: action.payload,
                isFetching: false,
            };
        case GET_MENU_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true,
            };
        default:
            return state;
    }
}