/**
 * Order data reducer
 * @author lgutierrez
 */

import {
    SELECT_ORDER,
    SELECT_ORDER_SUCCESS,
    SELECT_ORDER_FAILURE,
    CONFIRM_ORDER,
    CONFIRM_ORDER_SUCCESS,
    CONFIRM_ORDER_FAILURE,
    WILL_ADD_EXTRA_INGREDIENT,
    ADD_EXTRA_INGREDIENT,
} from './../constants';

const initialState =  {
    currentOrder: {},
    confirmedOrder: {},
    willAddExtraIngredient: false,
    isLoading: false,
    error: false,
}

export default orderReducer = (state = initialState, action) => {
    switch(action.type) {
        case SELECT_ORDER:
            return {
                ...state,
                currentOrder: action.payload,
                isLoading: true,
            };
        case SELECT_ORDER_SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case SELECT_ORDER_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: true,
            };
        case CONFIRM_ORDER:
            return {
                ...state,
                confirmedOrder: action.payload,
                isLoading: true,
            };
        case CONFIRM_ORDER_SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case CONFIRM_ORDER_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: true,
            };
        case WILL_ADD_EXTRA_INGREDIENT:
            return {
                ...state,
                willAddExtraIngredient: true,
            };
        case ADD_EXTRA_INGREDIENT:
            return {
                ...state,
                currentOrder: {
                    ...state.currentOrder,
                    name: state.currentOrder.name + ' / ' + action.payload.name,
                    price: (state.currentOrder.price / 2) + (action.payload.price / 2)
                },
            };
        default:
            return state;
    }
}