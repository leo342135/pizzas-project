/**
 * Api endpoint handling
 * @author lgutierrez
 */
const URL = 'https://static.mozio.com/mobile/tests/pizzas.json';

export const fetchMenuFromApi = () => {
    return fetch(URL)
    .then(Response =>{
        return Promise.all([Response, Response.json()])
    })
}